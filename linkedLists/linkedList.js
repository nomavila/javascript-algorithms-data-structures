//create a new Node = new node()
//create a new list = newlist

class Node {
    constructor(num) {
        this.number = num;
        this.next = null;
    }
}

class LinkedList {
    constructor(head = null, tail = null) {
        this.head = head;
        this.tail = tail;
    }
    //private modifier becuase an existing or use a check to run only if there is no node
    addNode(num) {
        let node = new Node(num);
        //if head is null that means tail is null so no need ot check if tail is null- NOT ?
        if (!this.head) {
            this.head = node;
            this.tail = node;
        }
    }

    appendNode(num) {
        let node = new Node(num);
        if (!this.head) {
            this.addNode(num);
        } else {
            this.tail.next = node;
            this.tail = node;
            // node.next = this.head;
            // this.head = node;
        }
    }

    prependNode(num) {
        let node = new Node(num);

        if (!this.head) {
            this.addNode(num);
        } else { 
            node.next = this.head;
            this.head = node;
        }
    }

    printList() {
        // console.log(this.head.next);
        let current = this.head;
        while(current) {
            console.log(current.number);
            current = current.next;
        }
    }

    length() {
        let current = this.head;
        let traversedTimes = 0
        while(current) {
            traversedTimes++;
            current = current.next;
        }
        return traversedTimes;
    }

    turnToArray() {
        let current = this.head;
        let myArray = [];
        while(current) {
            myArray.push(current.number);
            current = current.next;
        }
        return myArray;
    }

    removeFirstNode() {
        if (!this.head) {
            return;
        } else {
            this.head = this.head.next;
        }
    }
}

linkedList = new LinkedList();
linkedList.addNode(2);
linkedList.appendNode(3);
linkedList.appendNode(4);
linkedList.appendNode(5);
linkedList.appendNode(6);
linkedList.prependNode(1);
linkedList.prependNode(0);
// console.log(linkedList);
linkedList.printList();
console.log(linkedList.turnToArray());
console.log(linkedList.length());
linkedList.removeFirstNode();
linkedList.printList();